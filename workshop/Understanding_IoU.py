#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import segmentation_models as sm
import albumentations as A
import matplotlib.pyplot as plt
import pickle
from datetime import datetime
from sklearn.metrics import confusion_matrix
from sklearn.metrics import jaccard_score
sm.set_framework('tf.keras')
sm.framework()
print(tf.__version__)
print(tf.keras.__version__)


# In[2]:


y_true = np.array([
    [
        [0., 0., 0., 0., 0., 0., 1.],
        [0., 0., 0., 0., 0., 0., 1.],
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.]
    ],[
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.]
    ],[
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 1.],
        [0., 0., 0., 0., 0., 0., 1.]
    ]
], dtype="float32")


# In[3]:


y_true.shape


# In[4]:


y_preds = np.array([
    [
        [1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0.]
    ],[
        [1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0.]
    ],[
        [0., 0., 0., 0., 1., 0., 0.],
        [0., 0., 0., 0., 1., 0., 0.],
        [0., 0., 0., 0., 0., 1., 0.],
        [1., 0., 0., 0., 0., 0., 0.]
    ]
], dtype="float32")


# In[5]:


y_preds.shape


# In[6]:


palette = np.array([[  0, 255, 255],
                   [255, 255,   0],
                   [255,   0, 255],
                   [  0, 255,   0],
                   [  0,   0, 255],
                   [255, 255, 255],
                   [  0,   0,   0]])  


# In[7]:


plt.figure(figsize=(13, 13))
plot_number = 1
for i in range(7):
  plt.subplot(4, 4, plot_number)
  plt.xticks([])
  plt.yticks([])
  plt.title("true {}".format(i))
  plt.imshow(y_true[:,:,i], cmap="binary")
  plot_number += 1
  plt.subplot(4, 4, plot_number)
  plt.xticks([])
  plt.yticks([])
  plt.title("preds {}".format(i))
  plt.imshow(y_preds[:,:,i], cmap="binary")
  plot_number += 1
plt.show()


# In[8]:


r_y_true = np.argmax(y_true, axis=2)
r_y_true


# In[9]:


r_y_preds = np.argmax(y_preds, axis=2)
r_y_preds


# In[10]:


tf_y_true = tf.expand_dims(y_true, axis=0)
tf_y_preds = tf.expand_dims(y_preds, axis=0)


# In[11]:


from sklearn.metrics import confusion_matrix  
import numpy as np

def compute_iou(y_pred, y_true):
    # ytrue, ypred is a flatten vector
    y_pred = y_pred.flatten()
    y_true = y_true.flatten()
    current = confusion_matrix(y_true, y_pred, labels=[0, 1])
    # compute mean iou
    intersection = np.diag(current)
    ground_truth_set = current.sum(axis=1)
    predicted_set = current.sum(axis=0)
    union = ground_truth_set + predicted_set - intersection
    IoU = intersection / union.astype(np.float32)
    # return np.mean(IoU)
    return np.nanmean(IoU)


# In[12]:


def my_IoU(prediction, target):
    intersection = np.logical_and(target, prediction)
    union = np.logical_or(target, prediction)
    iou_score = np.sum(intersection) / np.sum(union)
    if str(iou_score) == "nan":
        return 0.0
    return iou_score


# # loop all
# 

# In[13]:


import pprint as pp
pp.pprint("dfsd")


# In[15]:


ious = {}
ious["jaccard_score"] = []
ious["jaccard_score mean"] = []
ious["jaccard_score micro"] = []
ious["jaccard_score macro"] = []
ious["sm_iou_score"] = []
ious["sm_iou_score threshold=0.5"] = []
for i in range(7):
    print("layer", i)
    target = y_true[:,:,i].flatten()
    prediction = y_preds[:,:,i].flatten()
    print("  ", target)
    print("  ", prediction)
    plt.subplot(1, 2, 1)
    plt.imshow(y_true[:,:,i], cmap="binary")
    plt.title("y_true {}".format(i))
    plt.subplot(1, 2, 2)
    plt.imshow(y_preds[:,:,i], cmap="binary")
    plt.title("y_preds {}".format(i))
    plt.show()
    # print(confusion_matrix(target, prediction,labels=[0, 1]))
    ious["jaccard_score"].append(list(jaccard_score(target, prediction, average=None, zero_division=1.0)))
    ious["jaccard_score mean"].append(np.mean(jaccard_score(target, prediction, average=None, zero_division=1.0)))
    ious["jaccard_score micro"].append(jaccard_score(target, prediction, average="micro", zero_division=1.0))
    ious["jaccard_score macro"].append(jaccard_score(target, prediction, average="macro", zero_division=1.0))
    sm_iou_score = sm.metrics.IOUScore()
    print("sm_iou_score (each time):",sm_iou_score(tf.expand_dims(y_true[:,:,i], axis=-1), tf.expand_dims(y_preds[:,:,i], axis=-1)))
    sm_iou_score = sm.metrics.IOUScore(threshold=0.5)
    print("sm_iou_score (each time, threshold=0.5):",sm_iou_score(tf.expand_dims(y_true[:,:,i], axis=-1), tf.expand_dims(y_preds[:,:,i], axis=-1)))
    sm_iou_score = sm.metrics.IOUScore()
    ious["sm_iou_score"].append(sm_iou_score(tf.expand_dims(y_true[:,:,i], axis=-1), tf.expand_dims(y_preds[:,:,i], axis=-1)))
    sm_iou_score = sm.metrics.IOUScore(threshold=0.5)
    ious["sm_iou_score threshold=0.5"].append(sm_iou_score(tf.expand_dims(y_true[:,:,i], axis=-1), tf.expand_dims(y_preds[:,:,i], axis=-1)))
    pp.pprint(ious)
    print("")

print("*"*50)              
pp.pprint(ious)
print("*"*50)  
print("""'binary':
    Only report results for the class specified by pos_label. This is applicable only if targets (y_{true,pred}) are binary.
'micro':
    Calculate metrics globally by counting the total true positives, false negatives and false positives.
'macro':
    Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
""")
print("jaccard_score 1 only", np.mean([x[1] for x in ious["jaccard_score"] if x!=0.0]))
print("jaccard_score 1 only sum", np.sum([x[1] for x in ious["jaccard_score"] if x!=0.0])/6)
print("jaccard_score mean", np.mean([x for x in ious["jaccard_score mean"] if x!=1.0]))
print("jaccard_score micro", np.mean([x for x in ious["jaccard_score micro"] if x!=1.0]))
print("jaccard_score macro", np.mean([x for x in ious["jaccard_score macro"] if x!=1.0]))
print("sm_iou_score np.mean", np.mean([x for x in ious["sm_iou_score"] if x!=1.0]))
print("sm_iou_score threshold=0.5 np.mean", np.mean([x for x in ious["sm_iou_score threshold=0.5"] if x!=1.0]))
sm_iou_score = sm.metrics.IOUScore()
print("sm_iou_score (no threshold):",sm_iou_score(y_true, y_preds))
sm_iou_score = sm.metrics.IOUScore(threshold=0.5)
print("sm_iou_score (threshold 0.5):",sm_iou_score(tf_y_true, tf_y_preds))

m = tf.keras.metrics.OneHotMeanIoU(num_classes=7)
m.update_state(y_true=y_true, y_pred=y_preds)
print("tf.keras.metrics.OneHotMeanIoU:", m.result().numpy())
m = tf.keras.metrics.MeanIoU(num_classes=7)
m.update_state(y_true=r_y_true, y_pred=r_y_preds)
print("tf.keras.metrics.MeanIoU:", m.result().numpy())

m = tf.keras.metrics.IoU(num_classes=7, target_class_ids=[0,1,2,3,4,5,6])
m.update_state(y_true=r_y_true, y_pred=r_y_preds)
print("tf.keras.metrics.IoU:", m.result().numpy())
                                 
# print("jaccard_score", jaccard_score(r_y_true, r_y_preds, average='samples'))


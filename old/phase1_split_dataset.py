#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
1st phase

Data preparation split into Train, Validation, Test

T    - V    - T
80 % - 10 % - 10%

"""

import os
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split


if __name__ == '__main__':
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    
    # Split the data
    train_val, test = train_test_split(metadata_patches_df, test_size=0.20, shuffle=True)
    val, test = train_test_split(test, test_size=0.50, shuffle=True)
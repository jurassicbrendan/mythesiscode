# -*- coding: utf-8 -*-
# from google.colab import drive
# drive.mount('/content/drive')

# !pip install --upgrade pip
# !pip install opencv-python-headless==4.1.2.30
# !pip install -U albumentations
# !pip install segmentation-models

"""

Data preparation by cutting images into many parts/patches.


Data preparation split into Train, Validation, Test
and train CNN model

T    - V    - T
80 % - 10 % - 10%

CNN: Unet
"""
                                    
import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tqdm import tqdm
import os
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import numpy as np
import segmentation_models as sm
import albumentations as A
import time
import matplotlib.pyplot as plt
import pickle

sm.set_framework('tf.keras')
sm.framework()
    

class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
    
def make_image_cuts(image_id, image_path, target_path, save_dir):
    patch_size = 256  # Size of the patches to be extract from the input images
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path))
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(target_path))
    # forest = np.zeros((mask.shape[0],mask.shape[1]) + (3,), dtype="uint8") + (0,255,0)
    # mask_forest = (mask == forest).all(axis=2,  keepdims=True).astype(int)
    # unique, counts  = np.unique(mask_forest.reshape(-1), axis=0, return_counts=True)
    # uc_dict = dict(zip(unique, counts))

    # if 1 not in uc_dict:
    #     print("\nbad", uc_dict)
    #     return [], []
    # if uc_dict[1] < 10000:
    #     print("\nbad", uc_dict)
    #     return [], []
    # print("\ngood", uc_dict)
    # images = tf.convert_to_tensor([image,mask])
    images = tf.convert_to_tensor(np.asarray([image,mask]))
    patches = Patches(patch_size)(images)
    image_patches_list = []
    mask_patches_list = []
    for k in [0,1]:
        for i, patch in enumerate(patches[k]): # images
            patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
            if k:
                save_name = "{}_{:02d}_mask.png".format(image_id, i)
                mask_patches_list.append(os.path.join(save_dir, save_name))
            else:
                save_name = "{}_{:02d}_sat.png".format(image_id, i)
                image_patches_list.append(os.path.join(save_dir, save_name))
            tf.keras.utils.save_img(
                path=os.path.join(save_dir, save_name), x=patch_img.numpy().astype("uint8")
            )
    assert len(image_patches_list) == len(mask_patches_list)
    return image_patches_list, mask_patches_list
       

def get_augmentation():
    return A.Compose([
        A.VerticalFlip(p=0.5),              
        A.RandomRotate90(p=0.5),
        ]
    )

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)

# Perform one hot encoding on label
def one_hot_encode(label, label_values):
    """
    Convert a segmentation image label array to one-hot format
    by replacing each pixel value with a vector of length num_classes
    # Arguments
        label: The 2D array segmentation image label
        label_values
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of num_classes
    """
    semantic_map = []
    for colour in label_values:
        equality = np.equal(label, colour)
        class_map = np.all(equality, axis = -1)
        semantic_map.append(class_map)
    semantic_map = np.stack(semantic_map, axis=-1)

    return semantic_map

# Perform reverse one-hot-encoding on labels / preds
def reverse_one_hot(image):
    """
    Transform a 2D array in one-hot format (depth is num_classes),
    to a 2D array with only 1 channel, where each pixel value is
    the classified class key.
    # Arguments
        image: The one-hot format image 
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of 1, where each pixel value is the classified 
        class key.
    """
    x = np.argmax(image, axis = -1)
    return x


class SatelliteImages(tf.keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self, batch_size, 
                 satellite_class_rgb_values, data, 
                 augmentation=None, preprocessing=None):
        self.patches_in_image = 81
        self.img_size = (256, 256)
        self.batch_size = batch_size
        self.batch_data_len = int(self.batch_size / self.patches_in_image)
        self.data = data
        self.augmentation = augmentation
        self.preprocessing = preprocessing
        self.satellite_class_rgb_values = satellite_class_rgb_values
        self.indices = np.arange(self.data.shape[0])


    def __len__(self):
        print("__len__ is:",(self.data.shape[0] * self.patches_in_image) // self.batch_size)
        return (self.data.shape[0] * self.patches_in_image) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        inds = self.indices[idx * self.batch_data_len:(idx + 1) * self.batch_data_len]
        batch_data = self.data.iloc[inds]
        
        batch_input_img_paths = batch_data["sat_image_path"]
        batch_target_img_paths = batch_data["mask_path"]
        
        x = np.zeros((self.batch_size,) +
                     self.img_size + (3,), 
                     dtype="float32")
        y = np.zeros((self.batch_size,) + 
                     self.img_size + (self.satellite_class_rgb_values.shape[0],), 
                     dtype="float32")
        patch_number = 0
        for num in range(self.batch_data_len):
            image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_input_img_paths.iloc[num]), dtype="uint8")
         
            mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_target_img_paths.iloc[num]), dtype="uint8")
            
            tf_img = tf.convert_to_tensor(np.asarray([image,mask]))
            patches = Patches(self.img_size[0])(tf_img)
            for p_num in range(self.patches_in_image):
                patch_img = tf.reshape(patches[0][p_num], self.img_size + (3,)).numpy()
                patch_mask = tf.reshape(patches[1][p_num], self.img_size + (3,)).numpy()
                if self.augmentation:
                    sample = self.augmentation(image=patch_img, mask=patch_mask)
                    patch_img, patch_mask = sample['image'], sample['mask']
                    
                if self.preprocessing:
                    sample = self.preprocessing(image=patch_img, mask=patch_mask)
                    patch_img, patch_mask = sample['image'], sample['mask']
                    
                patch_img = (patch_img / 255.0).astype("float")
                patch_mask = one_hot_encode(patch_mask, 
                                self.satellite_class_rgb_values).astype('float')
                x[patch_number] = patch_img
                y[patch_number] = patch_mask
                patch_number += 1
        print("patch_number returned:", patch_number)    
        return x, y
    
    def on_epoch_end(self):
        np.random.shuffle(self.indices)
        
def split_dataset(df):
    df = df.sample(20) # to make testing faster
    test_split = 0.2
    
    # Initial train and test split.
    train_df, test_df = train_test_split(
        df,
        test_size=test_split,
        # stratify=lyrics_df["Genre"].values,
        shuffle=True
    )
    
    # Splitting the test set further into validation
    # and new test sets.
    val_df = test_df.sample(frac=0.5)
    test_df.drop(val_df.index, inplace=True)
    
    return train_df, val_df, test_df
    
def make_model(train, val, save_directory, model_name, satellite_class_rgb_values):
    if not os.path.exists(os.path.join(save_directory, model_name)):
        os.mkdir(os.path.join(save_directory, model_name))
    full_directory = os.path.join(save_directory, model_name)
    
    batch_size = 81
    BACKBONE = 'resnet34'
    # BACKBONE = 'efficientnetb3'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    
    train_gen = SatelliteImages(
        batch_size=batch_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=train, 
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )
    val_gen = SatelliteImages(
        batch_size=batch_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=val, 
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )
    
    model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=7,
                  activation="softmax", encoder_freeze=True)

    optim = tf.keras.optimizers.Adam(0.001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    
    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
    
    model.compile(optim, total_loss, metrics)
    # model.summary()
   
    callbacks = [
    tf.keras.callbacks.ModelCheckpoint(os.path.join(full_directory, "best_model.h5"), save_best_only=True),
    tf.keras.callbacks.CSVLogger(os.path.join(full_directory, "logger.csv"), append=True),
    tf.keras.callbacks.EarlyStopping(patience=5, verbose=1),
    ]
    history = model.fit(
        train_gen, 
        epochs=50, 
        callbacks=callbacks, 
        validation_data=val_gen,
        # verbose=2
    )
    
    with open(os.path.join(full_directory, "history.pickle"), 'wb') as f:
        pickle.dump(history.history, f)
    return model

    

def load_original_classes():
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    
    class_dict = pd.read_csv(os.path.join(DATASET_DIR, 'class_dict.csv'))
    # Get class names
    satellite_class_list = class_dict['name'].tolist()
    # Get class RGB values
    class_rgb_values = class_dict[['r','g','b']].values.tolist()
        
    # Get RGB values of required classes
    satellite_class_indices = [satellite_class_list.index(cls.lower()) for cls in satellite_class_list]
    satellite_class_rgb_values =  np.array(class_rgb_values)[satellite_class_indices]
    
    return satellite_class_list, satellite_class_indices, satellite_class_rgb_values


def load_original_dataset():
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    replace_path = lambda x: os.path.join(DATASET_DIR, x)
    metadata_df = metadata_df.copy()
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].map(replace_path)
    metadata_df['mask_path'] = metadata_df['mask_path'].map(replace_path)

    return metadata_df

def load_model(model_path):
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    return tf.keras.models.load_model(model_path,
            custom_objects={
                'dice_loss_plus_1focal_loss': total_loss,
                'iou_score': sm.metrics.IOUScore(threshold=0.5),
                'f1-score': sm.metrics.FScore(threshold=0.5)
            })
            
def main():
    metadata_df = load_original_dataset()
    satellite_class_list, \
    satellite_class_insdices, \
    satellite_class_rgb_values = load_original_classes()
    
    train_df, val_df, test_df = split_dataset(metadata_df)
    
    m = make_model(train_df, val_df, "/home/kent/college/tmp/",
                   "model_Apr_12", satellite_class_rgb_values)
    
    # batch_size = 81
    # BACKBONE = 'resnet34'
    # preprocess_input = sm.get_preprocessing(BACKBONE)
    # test_gen = SatelliteImages(
    #     batch_size=batch_size, 
    #     satellite_class_rgb_values=satellite_class_rgb_values,
    #     data=test_df, 
    #     augmentation=None,
    #     preprocessing=get_preprocessing(preprocess_input),
    # )
    # test_gen[0]
    # m = load_model("/home/kent/college/Thesis/models/my_unet4.h5")

    # with open(os.path.join(save_directory, model_name + "_history.pickle"), 'rb') as f:
    #     history = pickle.load(f)
    #     plt.figure(figsize=(12, 5))
    #     plt.plot(history['iou_score'])
    #     plt.plot(history['val_iou_score'])
    #     plt.title('Model iou_score')
    #     plt.ylabel('iou_score')
    #     plt.xlabel('Epoch')
    #     plt.legend(['Train', 'Val'], loc='upper left')
    #     plt.show()
        
    #     plt.figure(figsize=(12, 5))
    #     plt.plot(history['loss'])
    #     plt.plot(history['val_loss'])
    #     plt.title('Model loss')
    #     plt.ylabel('Loss')
    #     plt.xlabel('Epoch')
    #     plt.legend(['Train', 'Val'], loc='upper left')
    #     plt.show()
            


    # print("Generate predictions")
    # p = m.predict(test_gen)
    # print("predictions shape:", p.shape)
    # print("Evaluate:")
    # scores = m.evaluate(test_gen)
    
if __name__ == '__main__':

    # pass
    main()
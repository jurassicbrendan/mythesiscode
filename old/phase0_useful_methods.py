#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Useful methods for Sateillte imaging project

"""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import os

# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(25, 16))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title() +"\n" +str(image.shape))
        plt.imshow(image.astype("uint8"))
    plt.show()
    
    
def show_image_and_mask(image_path, mask_path):
    visualize(
        image=tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path)), 
        mask=tf.keras.utils.img_to_array(tf.keras.utils.load_img(mask_path))
    )

def show_random_image_and_mask():
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    print(sample.values)
    visualize(
        image=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["sat_image_path"].values[0])),
        mask=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_path"].values[0])),
        image_patch=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["sat_image_patch"].values[0])),
        mask_patch=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_patch"].values[0]))
    )

def show_random_image_and_mask_with_patches():
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    visualize(
        image=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["sat_image_path"].values[0])),
        mask=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_path"].values[0])),
        image_patch=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["sat_image_patch"].values[0])),
        mask_patch=tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_patch"].values[0]))
    )
    sample_id = sample["image_id"].values[0]
    sample = metadata_patches_df[metadata_patches_df["image_id"]==sample_id]
    # print(sample.values)
    n = 9
    plt.figure(figsize=(10, 10))
    i = 0 
    for index, row in sample.iterrows():
        ax = plt.subplot(n, n, i + 1)
        patch_img = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            row["sat_image_patch"]))
        plt.imshow(patch_img.astype("uint8"))
        plt.axis("off")
        i += 1
    plt.show()
    plt.figure(figsize=(10, 10))
    i = 0 
    for index, row in sample.iterrows():
        ax = plt.subplot(n, n, i + 1)
        patch_img = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            row["mask_patch"]))
        plt.imshow(patch_img.astype("uint8"))
        plt.axis("off")
        i += 1
    plt.show()

def get_random_image():
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    return tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["sat_image_patch"].values[0]), dtype="uint8")

def get_random_mask():
    PATCHES_DIR = "/home/kent/college/Thesis/deepglobe/patches/"
    metadata_patches_df = pd.read_csv(os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    return tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_patch"].values[0]), dtype="uint8")

def get_random_patch_image_and_mask():
    PATCHES_DIR = "/tmp/patches/"
    metadata_patches_df = pd.read_csv(
        os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    return tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_patch"].values[0]), dtype="uint8"), tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_patch"].values[0]), dtype="uint8")

def get_random_image_and_mask():
    PATCHES_DIR = "/tmp/patches/"
    metadata_patches_df = pd.read_csv(
        os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    print(sample.to_dict())
    return tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_path"].values[0]), dtype="uint8"), tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_path"].values[0]), dtype="uint8")
                        
def show_random_image_and_mask():
    PATCHES_DIR = "/tmp/patches/"
    metadata_patches_df = pd.read_csv(
        os.path.join(PATCHES_DIR, 'metadata_patches.csv'))
    sample = metadata_patches_df.sample(1)
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_patch"].values[0]), dtype="uint8")
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            sample["mask_patch"].values[0]), dtype="uint8")
    forest = np.zeros((256,256) + (3,), dtype="uint8") + (0,255,0)
    mask_forest = (mask == forest).all(axis=2,  keepdims=True).astype(int)
    print(sample.to_dict())
    unique, counts  = np.unique(mask_forest.reshape(-1), axis=0, return_counts=True)
    visualize(
        image=image, 
        mask=mask,
        mask_forest=mask_forest,
    )
    uniques = [str(x) for x in unique] 
    plt.figure(figsize=(5, 5))
    plt.bar(uniques,counts)
    plt.ylabel('number of pixels')
    plt.xlabel('Classififcation')
    plt.title('images')
    plt.show()
                        
def see_augmented_images():
    data_augmentation = tf.keras.Sequential(
        [
            tf.keras.layers.RandomFlip("horizontal"),
            tf.keras.layers.RandomRotation(0.1),
            tf.keras.layers.RandomZoom(0.2),
        ]
    )
    # IMG_SIZE = 180
    # resize_and_rescale = tf.keras.Sequential([
    #   tf.keras.layers.Resizing(IMG_SIZE, IMG_SIZE),
    #   tf.keras.layers.Rescaling(1./255)
    # ])
    plt.figure(figsize=(8, 8))
    # image = get_random_image()
    scoby = "/home/kent/college/data_sets/scoby/IMG_20200709_192029_200.png"
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(scoby))
    for i in range(9):
        augmented_image = data_augmentation(image)
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(augmented_image.numpy().astype("uint8"))
        plt.axis("off")
    plt.show()

def plot_7_classes_proportions():
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    replace_path = lambda x: os.path.join(DATASET_DIR, x)
    metadata_df = metadata_df.copy()
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].map(replace_path)
    metadata_df['mask_path'] = metadata_df['mask_path'].map(replace_path)
    first = True
    matrixArr1 = None
    unique = None
    counts = None
    total_len = len(metadata_df)
    classes = ['urban_land',
    'agriculture_land',
    'rangeland',
     'forest_land',
     'water',
     'barren_land',
     'unknown']
    class_dict = dict(zip(classes, [0,0,0,0,0,0,0]))
    select_class_dict = get_class_dict()
    for index, row in metadata_df.iterrows():
        # row["mask_path"]
        # img = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        #     os.path.join(DIR, row["mask_path"]), color_mode="grayscale"), dtype="uint8")
        img = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
            row["mask_path"]), dtype="uint8")
        img_reshape = img.reshape(-1,3)
        if not first:
            matrixArr1 = np.append(matrixArr1, img_reshape , axis=0)
            if index % 10 == 0:
                print(index, "of", total_len)
            if index % 200 == 0:
                unique, counts  = np.unique(matrixArr1, axis=0, return_counts=True)
                uniques = [select_class_dict[str(list(x))] for x in unique] 
                for i, uni in enumerate(uniques):
                    class_dict[uni] += counts[i]                
                matrixArr1 = img_reshape
                print(class_dict)
                # break
        else:
            matrixArr1 = img_reshape
            first = False
    unique, counts  = np.unique(matrixArr1, axis=0, return_counts=True)
    uniques = [select_class_dict[str(list(x))] for x in unique] 
    for i, uni in enumerate(uniques):
        class_dict[uni] += counts[i]                
    matrixArr1 = img_reshape
    print(class_dict)
                
    # unique, counts  = np.unique(matrixArr1, axis=0, return_counts=True)
    # select_class_dict = get_class_dict()
    # uniques = [select_class_dict[str(list(x))] for x in unique] 
    class_df  = pd.DataFrame({"class":class_dict.keys(), "counts": class_dict.values()})
    class_df
    class_df['class'] = pd.Categorical(class_df['class'], ['urban_land',
    'agriculture_land',
    'rangeland',
     'forest_land',
     'water',
     'barren_land',
     'unknown'])
    class_df
    class_df = class_df.sort_values("class")
    
    class_df = pd.read_csv("class_df.csv")
    import matplotlib.pyplot as plt

    plt.style.use('seaborn-whitegrid')
    # import matplotlib.pyplot as plt
    # import matplotlib.pyplot as plt
    # plt.style.reload_library()
    
    plt.rcParams['figure.dpi'] = 150


    #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams.update({'font.size': 13})
    from matplotlib.ticker import FuncFormatter
    def millions(x, pos):
        'The two args are the value and tick position'
        return '%1.1f B' % (x*1e-9)
    formatter = FuncFormatter(millions)

    # plt.figure(figsize=(12, 8))
    fig, ax = plt.subplots(figsize=(12,7))
    ax.yaxis.set_major_formatter(formatter)
    barlist = plt.bar("class","counts", data=class_df)
    for i, bar in enumerate(barlist):
        bar.set_color(satellite_class_rgb_values[i]/255)
        bar.set_edgecolor("black")
        bar.set_linewidth(1)
    plt.ylabel('Number of pixels')
    plt.xlabel('Label')
    plt.title('Total number of Pixels in 803 images per label in Billions')
    plt.show()
    # plt.savefig("pixels_per_label.png",dpi=300)
    
    # import pickle
    # with open('unique_counts.pickle', 'wb') as f:
    #     pickle.dump([unique, counts], f)
    # plt.figure(figsize=(8, 8))
    # # image = get_random_image()
    # scoby = "/home/kent/college/data_sets/scoby/IMG_20200709_192029_200.png"
    # image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(scoby))
    # for i in range(9):
    #     augmented_image = data_augmentation(image)
    #     ax = plt.subplot(3, 3, i + 1)
    #     plt.imshow(augmented_image.numpy().astype("uint8"))
    #     plt.axis("off")
    # plt.show()
    
    # image1 = tf.keras.utils.load_img("/home/kent/college/data_sets/camvid/0001TP_006690_mask.png")

def get_class_dict():
    DIR = "/home/kent/college/Thesis/deepglobe/"
    class_dict = pd.read_csv(os.path.join(DIR, 'class_dict.csv'))
    # Get class names
    select_classes = class_dict['name'].tolist()
    # Get class RGB values
    class_rgb_values = [str(x) for x in class_dict[['r','g','b']].values.tolist()]
    select_class_dict = dict(zip(class_rgb_values,select_classes))
    return select_class_dict

    
def show_one_class():
    forest = np.zeros((256,256) + (3,), dtype="uint8") + (0,255,0)
    plt.figure(figsize=(10, 6))
    for i in range(0,10,2):
        plt.subplot(5, 2, i + 1)
        image, mask = get_random_image_and_mask()
        mask = (mask == forest).all(axis=2,  keepdims=True)
        plt.imshow(image)
        plt.axis("off")
        plt.subplot(5, 2, i + 2)
        plt.imshow(mask)
        plt.axis("off")
    plt.show()

def show_one_class2():
    forest = np.zeros((2448,2448) + (3,), dtype="uint8") + (0,255,0)
    # forest = np.zeros((256,256) + (3,), dtype="uint8") + (0,255,0)
    for i in range(10):
        image, mask = get_random_image_and_mask()
        # image, mask = get_random_patch_image_and_mask()
        mask_forest = (mask == forest).all(axis=2,  keepdims=True)
        img_reshape = mask.reshape(-1,3)
        unique, counts  = np.unique(img_reshape, axis=0, return_counts=True)
        select_class_dict = get_class_dict()
        uniques = [select_class_dict[str(list(x))] for x in unique] 
        plt.figure(figsize=(8, 6))
        plt.bar(uniques,counts)
        plt.ylabel('number of pixels')
        plt.xlabel('Classififcation')
        plt.title('Classififcations')
        plt.show()
        visualize(
            image=image, 
            mask=mask,
            mask_forest=mask_forest
        )
    
def show_tf_layers(m):
    for layer in m.layers:
        print(
            f"{layer.trainable}\t\t\
                {layer.name}\t\
                    {layer.__class__.__name__}\t\
                        {layer.output_shape}")
